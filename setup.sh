#!/usr/bin/env bash

############################
#  SUDO
############################

# mise à jour et install sudo
apt-get -qy install sudo  2>&1 | logger -s -t 'setup.sh'
# a améliorer la vérificaiton de sudo

############################
#  UPDATE
############################

sudo apt-get -qy dist-upgrade 2>&1 | logger -s -t 'setup.sh' && \
    sudo apt-get -y autoremove 2>&1 | logger -s -t 'setup.sh' && \
    sudo apt-get -y clean 2>&1 | logger -s -t 'setup.sh' && \
    sudo apt-get -fy install 2>&1 | logger -s -t 'setup.sh'

############################
#  GIT
############################

# mise à jour et install git
sudo apt-get -qy install git  2>&1 | logger -s -t 'setup.sh'
# configuration de git
git config --global user.email "$USER@$HOSTNAME" 2>&1 | logger -s -t 'setup.sh'
git config --global user.name  "$USER@$HOSTNAME" 2>&1 | logger -s -t 'setup.sh'

# création d'une clé rsa
[ ! -f $HOME/.ssh/id_rsa ] && \
    ssh-keygen -t rsa -f $HOME/.ssh/id_rsa -q -P "" 2>&1 | logger -s -t 'setup.sh'
ssh-keyscan -t rsa,dsa gitlab.com  >> ~/.ssh/known_hosts


# installation de ce package pour avoir add-apt-repository
sudo apt-get -qy install software-properties-common  2>&1 | logger -s -t 'setup.sh'


############################
#  VIM
############################

sudo apt-get -qy install vim  2>&1 | logger -s -t 'setup.sh'

############################
#  SSHFS 
############################

sudo apt-get -qy install sshfs  2>&1 | logger -s -t 'setup.sh'

############################
#  LOCALE A FAIRE
############################


############################
#  KEYBOARD A FAIRE
############################

mkdir -p $HOME/dockworker; cd $HOME/dockworker
git clone https://gitlab.com/dockworker/s_postinstallation.git

s_postinstallation/profile_maj.sh
