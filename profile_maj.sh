DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# copie les préférences pour les raccourcis shell et la coloration
cp $DIR/profile/.bash_login $HOME/.bash_login
cp $DIR/profile/.bash_aliases $HOME/.bash_aliases

# copie les préférences pour vim
cp $DIR/profile/.vimrc $HOME/.vimrc

# rends non sensible à la casse l'autocomplétion
LINE='set completion-ignore-case on'
FILE=$HOME/.inputrc
grep -q "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
# 
LINE='set meta-flag on'
FILE=$HOME/.inputrc
grep -q "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
# 
LINE='set input-meta on'
FILE=$HOME/.inputrc
grep -q "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
# 
LINE='set convert-meta off'
FILE=$HOME/.inputrc
grep -q "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
# 
LINE='set output-meta on'
FILE=$HOME/.inputrc
grep -q "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
# 
LINEsearch='beginning-of-line'
LINE='"\e\[1~": beginning-of-line     # Home key'
FILE=$HOME/.inputrc
grep -q "$LINEsearch" "$FILE" || echo "$LINE" >> "$FILE"
# 
LINEsearch='end-of-line'
LINE='"\e\[4~": end-of-line           # End key'
FILE=$HOME/.inputrc
grep -q "$LINEsearch" "$FILE" || echo "$LINE" >> "$FILE"
# 
LINEsearch='beginning-of-history'
LINE='"\e\[5~": beginning-of-history  # PageUp key'
FILE=$HOME/.inputrc
grep -q "$LINEsearch" "$FILE" || echo "$LINE" >> "$FILE"
# 
LINEsearch='end-of-history        # PageDown key'
LINE='"\e\[6~": end-of-history        # PageDown key'
FILE=$HOME/.inputrc
grep -q "$LINEsearch" "$FILE" || echo "$LINE" >> "$FILE"
# 
LINEsearch='delete-char           # Delete key'
LINE='"\e\[3~": delete-char           # Delete key'
FILE=$HOME/.inputrc
grep -q "$LINEsearch" "$FILE" || echo "$LINE" >> "$FILE"
# 
LINEsearch='quoted-insert         # Insert key'
LINE='"\e\[2~": quoted-insert         # Insert key'
FILE=$HOME/.inputrc
grep -q "$LINEsearch" "$FILE" || echo "$LINE" >> "$FILE"
# 
LINEsearch='backward-word          # Ctrl + Left Arrow key'
LINE='"\eOD": backward-word          # Ctrl + Left Arrow key'
FILE=$HOME/.inputrc
grep -q "$LINEsearch" "$FILE" || echo "$LINE" >> "$FILE"
# commentaires à faire
LINEsearch='forward-word           # Ctrl + Right Arrow key'
LINE='"\eOC": forward-word           # Ctrl + Right Arrow key'
FILE=$HOME/.inputrc
grep -q "$LINEsearch" "$FILE" || echo "$LINE" >> "$FILE"


#installation screenfetch
# Detect the platform (similar to $OSTYPE)
OS="`uname`"
case $OS in
  'Linux')
    OS='Linux'
    [[ $(screenfetch -V) != *screenFetch* ]] && sudo apt-add-repository -y ppa:djcj/screenfetch
    [[ $(screenfetch -V) != *screenFetch* ]] && sudo apt-get update
    [[ $(screenfetch -V) != *screenFetch* ]] && sudo apt-get install -qy screenfetch
    screenfetch
    ;;
  'FreeBSD')
    OS='FreeBSD'
    ;;
  'WindowsNT')
    OS='Windows'
    ;;
  'Darwin')
    OS='Mac'
    # commentaires à faire
    LINEsearch='bash_aliases'
    LINE="if [ -f ~/.bash_aliases ]; then\n  source ~/.bash_aliases\nfi"
    FILE=$HOME/.bash_profile
    grep -q "$LINEsearch" "$FILE" || echo -e "$LINE" >> "$FILE"

    #install brew
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null
    
    #install tools 
    brew cask install appcleaner
    brew cask install angry-ip-scanner

    ;;
  'SunOS')
    OS='Solaris'
    ;;
  'AIX') ;;
  *) ;;
esac


