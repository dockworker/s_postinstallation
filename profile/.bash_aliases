#!/bin/bash

export ATV_BASH_ALIASES="ATV"

alias ls='ls $LS_OPTIONS'
alias ll='ls $LS_OPTIONS -l'
alias l='ls $LS_OPTIONS -la'
alias du="du -ach | sort -h"
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"
alias myip="curl http://ipecho.net/plain; echo"

##################################
#        FUNCTION                #
##################################

atv_history(){
   history | grep -vE -e "[0-9]{1,4}  hy |history" | grep -iE -e $1 | less
}
export -f atv_history
alias ahistory='atv_history'

atv_ps(){
    ps auxwf | grep -i "$1" | grep -vi "grep -i $1" | sort
}
export -f atv_ps
alias aps='atv_ps'

atv_gitpm(){
    git add -A
    git commit -a -m "'$*'"
    git push origin master
}
export -f atv_gitpm
alias agitpm='atv_gitpm'

atv_gitpd(){
    git add -A
    git commit -a -m "'$*'"
    git push origin develop
}
export atv_gitpd
alias agitpd='atv_gitpd'

atv_docker_flush(){
    # Delete all containers
    docker rm $(docker ps -a -q)
    # Delete all images
    docker rmi $(docker images -q)
}
export atv_docker_flush
alias adockerflush='atv_docker_flush'

atv_docker_clean(){
    docker rm $(docker ps -qa --no-trunc --filter "status=exited")
    docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
}
export atv_docker_clean
alias adockerclean='atv_docker_clean'


echo ".bash_aliases ok"
alias

if [ -f .bash_login ] && [ "$ATV_BASH_LOGIN" != 'ATV' ]; then
    . .bash_login
elif [ ! -f .bash_login ]; then
    echo ".bash_login non présent"
fi
history | awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}' | grep -v "./" | column -c3 -s " " -t | sort -nr | nl |  head -n10

OS="`uname`"
case $OS in
  'Linux')
    OS='Linux'
    screenfetch
    ;;
  'FreeBSD')
    OS='FreeBSD'
    ;;
  'WindowsNT')
    OS='Windows'
    ;;
  'Darwin')
    OS='Mac'
#    screenfetch
    ;;
  'SunOS')
    OS='Solaris'
    ;;
  'AIX') ;;
  *) ;;
esac


