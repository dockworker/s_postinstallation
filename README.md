# Brief 

* installation de git et des préférences

# postinstallation script pour Mac OS X

* lancement du script profile_maj.sh dans le terminal

<pre>
wget https://gitlab.com/dockworker/s_postinstallation/raw/master/setup.sh
bash ./setup.sh
rm ./setup.sh
</pre>

# postinstallation script pour raspberry

<pre>
wget https://gitlab.com/dockworker/s_postinstallation/raw/master/setup.sh
bash ./setup.sh
rm ./setup.sh
</pre>

# postinstallation script pour kimsufi DEBIAN 8

## 1. Sélection template

  * Choisir le template Debian 8 
  * Laisser Anglais (inutile de passer un serveur en Français)
  * Cocher Installation personnalisée
  * Cliquer sur Suivant

![alt text](README.images/01-selection-template.jpg "selection template")

## 2. Partitionnement

  * Pour le partitionnement, supprimer la partition /home
  * Cliquer sur Suivant

![alt text](README.images/02-partition-supprimer-home.jpg "selection template")

## 3. Options et postinstallation

  * Remplir le nom d'hôte de votre serveur
  * Sélectionner votre clé SSH
  * coller le script de post installation suivant : 
  * https://gitlab.com/dockworker/s_postinstallation/raw/master/setup.sh
  * Cliquer sur Suivant

![alt text](README.images/03-options-avec-postinstallation.jpg "selection template")

## 4. confirmer et attendre
